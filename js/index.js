         $(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
            $('.carousel').carousel({
                interval:3000
            });
            console.log("Pruebas");
            $("#modalContacto").on('show.bs.modal', function (e) {
               console.log("El modal contacto se está mostrando.");
               $('#contactoBtn').removeClass('btn-outline-success');
               $('#contactoBtn').addClass('btn-primary');
               $('#contactoBtn').prop('disabled',true);
           });
            $("#modalContacto").on('shown.bs.modal', function (e) {
               console.log("El modal contacto se mostró.");
           });
            $('#modalContacto').on('hide.bs.modal', function (e) {
               console.log('El modal contacto se está ocultando.');
               $('#contactoBtn').removeClass('btn-primary');
               $('#contactoBtn').addClass('btn-outline-success');
               $('#contactoBtn').prop('disabled',false);

           });
            $('#modalContacto').on('hidden.bs.modal', function (e) {
               console.log('El modal contacto se ocultó.');
           });

        });